# Tipu API #

This API provides easy access of CRUD functionality to the back end of the Tipu system. The server operates on port `8080` but

# Documentation #
[API Docs](https://documenter.getpostman.com/view/2821044/RWEgsKJo)

# Dependancies #
 > `node.js` `nginx (or equivilent)` `pm2`

# Deploymemt #

 1. Navigate to the folder to deploy in
 2. Clone the repository with `git clone https://gitlab.com/DeloitteTipu/TipuAPI` and enter your credentials
 3. Enter the TipuAPI folder with `cd TipuAPI` and install the node dependancies with `npm install`
 4. Create a folder called `config` with a file inside called `config.js` pasting the following code with your database credentials inside
 ```js
 database_info = {};
    
    database_info.host = '';
    database_info.port = 3306;
    database_info.database = '';
    database_info.username = '';
    database_info.password = '';
    
    module.exports = database_info;
```
 5. Run `sudo chmod +x tipu_api.js` to allow the server to be executed
 6. Add a reverse proxy in nginx or your choice of web server pointing to port `8080`
 7. Start the server with `pm2 tipu_api.js`
 
# Contributing

 > [How to contribute](https://gitlab.com/DeloitteTipu/tipu_backend/blob/master/CONTRIBUTING.md)