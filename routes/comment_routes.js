const commentUtil    = require('./util/comment_util');

module.exports = function (app, db, version_slug) {
    app.post(version_slug + '/:target_type/:target_id/comment', function (req, res) {
        if (['user', 'goal', 'review'].indexOf(req.params.target_type) >= 0) {
            console.log(req.params.target_id);
            commentUtil.createComment(db, req.params.target_type, req.params.target_id, req.body).then(function (result) {
                res.send(result);
            }).catch(function (err) {
                res.send(err.code);
            })
        }
    });

    app.put(version_slug + '/:target_type/:target_id/comment/:comment_id', function (req, res) {
        if (['user', 'goal', 'review'].indexOf(req.params.target_type) >= 0) {
            commentUtil.updateComment(db, req.params.target_type, req.params.target_id, req.params.comment_id, req.body).then(function (result) {
                res.send(result);
            }).catch(function (err) {
                res.send(err.code);
            })
        }
    });

    app.get(version_slug + '/:target_type/:target_id/comment', function (req, res) {
        commentUtil.getComments(db, req.params.target_type, req.params.target_id, req.query).then(function (result) {
            res.send(result);
        }).catch(function (err) {
            res.send(err.code);
        })
    });

    app.delete(version_slug + '/:target_type/:target_id/comment/:comment_id', function (req, res) {
        commentUtil.deleteComment(db, req.params.comment_id).then(function (result) {
            res.send(result.affectedRows > 0 ? {deleted: true} : {deleted: false});
        }). catch(function (err) {
            res.send(err.code);
        })
    })
};