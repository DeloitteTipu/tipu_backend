const goalUtil    = require('./util/goal_util');

module.exports = function(app, db, version_slug) {

    /**
     * Gets all user data and returns it as a list of dictionaries
     */
    app.get(version_slug + '/goal', function (req, res) {

        goalUtil.getGoal(db, req.query).then(function (result) {
            res.send(result);
        }).catch(function (err) {
            res.send(err.code)
        })
    });

    /**
     * Creates a new goal from the JSON in the POST body
     */
    app.post(version_slug + '/goal', function(req, res) {
        goalUtil.createGoal(db, req.body).then(function (result) {
            res.send(result)
        }).catch(function (err) {
            res.send(err.code)
        })
    });

    /**
     * Allows for user data in the MySQL table to be updated. The url will target a specific user
     * and the JSON in the body will be used to construct a query which will then execute.
     *
     * If any column is referenced that doesn't exist a MySQL error will occur
     */
    app.put(version_slug + '/goal/:id', function (req, res) {
        goalUtil.updateGoal(db, req.params.id, req.body).then(function (result) {
            res.send(result);
        }).catch(function (err) {
            res.send(err.code);
        })
    });

    app.delete(version_slug + '/goal/:id', function (req, res) {
        goalUtil.deleteGoal(db, req.params.id).then(function (result) {
            res.send(result.affectedRows > 0 ? {deleted: true} : {deleted: false});
        }).catch(function (err) {
            res.send(err.code)
        })
    })
};