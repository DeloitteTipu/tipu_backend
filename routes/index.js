const userRoutes = require('./user_routes');
const goalRoutes = require('./goal_routes');
const reviewRoutes = require('./review_routes');
const commentRoutes = require('./comment_routes');
const statsRoutes = require('./stats_routes');

module.exports = function(app, db) {
    let version_slug = '/api/v1';

    userRoutes(app, db, version_slug);
    goalRoutes(app, db, version_slug);
    reviewRoutes(app, db, version_slug);
    commentRoutes(app, db, version_slug);
    statsRoutes(app, db, version_slug);
};
