const statsUtil    = require('./util/stats_util');

module.exports = function (app, db, version_slug) {


    app.get(version_slug + '/stats/average_rating', function (req, res) {

        statsUtil.averageRatings(db, req.query).then(function (result) {
            res.send(result);
        }).catch(function (err) {
            res.send(err.code)
        })
    });

    app.get(version_slug + '/stats/rating_over_time', function (req, res) {

        statsUtil.ratingOverTime(db, req.query).then(function (result) {
            const categories = result.map(e => e['categories']);
            const data = result.map(e => e['data']);
            res.send({categories: categories, data: data});
        }).catch(function (err) {
            res.send(err.code)
        })
    });

    app.get(version_slug + '/stats/goal_completion', function (req, res) {

        statsUtil.goalsCompletion(db, req.query).then(function (result) {
            res.send(result);
        }).catch(function (err) {
            res.send(err.code)
        })
    });
};