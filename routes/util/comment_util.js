module.exports = {
    createComment: function (db, targetType, targetId, commentData) {
        console.log(targetId);
        return new Promise(function (resolve, reject) {

            let queryString = 'INSERT INTO COMMENT (target_type, target_id';

            // commentData.push({"targetType": targetType});
            console.log(commentData);

            for (let i = 0; i < Object.keys(commentData).length; i++) {
                let key = Object.keys(commentData)[i];

                queryString += ', ' + key;
            }

            queryString += ') VALUES (';

            for (let i = 0; i < Object.values(commentData).length; i++) {

                if (i === 0) {
                    queryString += '?, ?, ?';
                }

                if (i > 0) {
                    queryString += ', ?';
                }
            }

            queryString += ');';

            db.query(queryString, [targetType, targetId].concat(Object.values(commentData)), function (err, rows) {
                if (!err) {
                    // let user = module.exports.getGoal(db, {id: rows.insertId + ''});
                    resolve({'made': true})

                } else {
                    reject(err)

                }
            });

        })
    },

    deleteComment: function(db, commentId) {
        return new Promise(function (resolve, reject) {

            db.query('DELETE FROM COMMENT WHERE id = ?', commentId, function (err, result) {
                if (!err) {
                    resolve(result);
                } else {
                    reject(err);
                }
            });
        });
    },

    getComments: function (db, targetType, targetId, filter) {
        return new Promise(function (resolve, reject) {
            let queryString = 'SELECT * FROM COMMENT WHERE ';

            for (let i = 0; i < Object.keys(filter).length; i++) {
                let key = Object.keys(filter)[i];
                let value = Object.values(filter)[i];

                queryString += key + ' LIKE ? AND ';
            }

            queryString += 'target_type=? AND target_id=?';

            let values = Object.values(filter);
            values.push(targetType);
            values.push(targetId);

            console.log(values);
            console.log(queryString);

            db.query(queryString, values, function (err, result, fields) {
                if (!err) {
                    resolve(result);
                } else {
                    reject(err);
                }
            });
        })
    },

    updateComment: function (db, targetType, targetId, commentId, updates) {
        return new Promise(function (resolve, reject) {
            let valuesString = '';
            let responseId = commentId;

            for (const [key, value] of Object.entries(updates)) {
                valuesString += ' ' + key + '=?,';

                if (key === "id") {
                    responseId = key;
                }
            }

            const queryString = 'UPDATE COMMENT SET' + valuesString.slice(0, -1) + ' WHERE id=?';

            console.log(queryString);

            let values = Object.values(updates);
            values.push(commentId);

            console.log(values);

            db.query(queryString, values, function (err, result) {
                if (!err) {
                    let comment = module.exports.getComments(db, targetType, targetId, {id: responseId});
                    resolve(comment)
                } else {
                    reject(err);
                }
            });
        });
    }
};