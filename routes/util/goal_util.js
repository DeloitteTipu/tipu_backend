module.exports = {
    createGoal: function (db, goalData) {
        return new Promise(function (resolve, reject) {

            let queryString = 'INSERT INTO GOAL (';

            for (let i = 0; i < Object.keys(goalData).length; i++) {
                let key = Object.keys(goalData)[i];

                if (i === 0) {
                    queryString += key;
                }

                if (i > 0) {
                    queryString += ', ' + key;
                }
            }

            queryString += ') VALUES (';

            for (let i = 0; i < Object.values(goalData).length; i++) {

                if (i === 0) {
                    queryString += '?';
                }

                if (i > 0) {
                    queryString += ', ?';
                }
            }

            queryString += ');';

            db.query(queryString, Object.values(goalData), function(err, rows) {
                if (!err) {
                    let user = module.exports.getGoal(db, {id: rows.insertId + ''});
                    resolve(user)

                } else {
                    reject(err)

                }
            });
        });
    },

    deleteGoal: function(db, goalId) {
        return new Promise(function (resolve, reject) {

            db.query('DELETE FROM GOAL WHERE id = ?', goalId, function (err, result) {
                if (!err) {
                    resolve(result);
                } else {
                    reject(err);
                }
            });
        });
    },

    getGoal: function (db, filter) {
        return new Promise(function(resolve, reject) {
            let queryString = 'SELECT * FROM GOAL';

            for (let i = 0; i < Object.keys(filter).length; i++) {
                let key = Object.keys(filter)[i];
                let value = Object.values(filter)[i];

                if (i === 0) {
                    queryString += ' WHERE ';
                }

                if (i > 0) {
                    queryString += ' AND ';
                }

                queryString += key + ' LIKE ?';
            }

            queryString += ' ORDER BY complete DESC, last_update ASC;';

            db.query(queryString, Object.values(filter), function (err, result, fields) {
                if (!err) {
                    resolve(result);
                } else {
                    reject(err);
                }
            });
        });
    },
    
    updateGoal: function (db, goalId, updates) {
        return new Promise(function (resolve, reject) {
            let valuesString = '';
            let responseId = goalId;



            for (const [key, value] of Object.entries(updates)) {
                valuesString += ' ' + key + '=?,';

                if (key === "id") {
                    responseId = key;
                }
            }

            const queryString = 'UPDATE GOAL SET' + valuesString.slice(0, -1) + ' WHERE id=?';

            let values = Object.values(updates);
            values.push(goalId);

            db.query(queryString, values, function (err, result) {
                if (!err) {
                    let goal = module.exports.getGoal(db, {id: responseId});
                    resolve(goal)
                } else {
                    reject(err);
                }
            });
        });
    }


};