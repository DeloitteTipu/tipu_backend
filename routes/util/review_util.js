module.exports = {
    createReview: function (db, reviewData) {
        return new Promise(function (resolve, reject) {

            let queryString = 'INSERT INTO REVIEW (';

            for (let i = 0; i < Object.keys(reviewData).length; i++) {
                let key = Object.keys(reviewData)[i];

                if (i === 0) {
                    queryString += key;
                }

                if (i > 0) {
                    queryString += ', ' + key;
                }
            }

            queryString += ') VALUES (';

            for (let i = 0; i < Object.values(reviewData).length; i++) {

                if (i === 0) {
                    queryString += '?';
                }

                if (i > 0) {
                    queryString += ', ?';
                }
            }

            queryString += ');';

            db.query(queryString, Object.values(reviewData), function(err, rows) {
                if (!err) {
                    let user = module.exports.getReview(db, {id: rows.insertId + ''});
                    resolve(user)

                } else {
                    reject(err)

                }
            });
        });
    },

    deleteReview: function(db, reviewId) {
        return new Promise(function (resolve, reject) {

            db.query('DELETE FROM REVIEW WHERE id = ?', reviewId, function (err, result) {
                if (!err) {
                    resolve(result);
                } else {
                    reject(err);
                }
            });
        });
    },

    getReview: function (db, filter) {
        return new Promise(function(resolve, reject) {
            let queryString = 'SELECT * FROM REVIEW';

            for (let i = 0; i < Object.keys(filter).length; i++) {
                let key = Object.keys(filter)[i];
                let value = Object.values(filter)[i];

                if (i === 0) {
                    queryString += ' WHERE ';
                }

                if (i > 0) {
                    queryString += ' AND ';
                }

                queryString += key + ' LIKE ?';
            }

            queryString += ' ORDER BY year DESC, end_of_year DESC;';

            db.query(queryString, Object.values(filter), function (err, result, fields) {
                if (!err) {
                    resolve(result);
                } else {
                    reject(err);
                }
            });
        });
    },

    updateReview: function (db, reviewId, updates) {
        return new Promise(function (resolve, reject) {
            let valuesString = '';
            let responseId = reviewId;

            for (const [key, value] of Object.entries(updates)) {
                valuesString += ' ' + key + '=?,';

                if (key === "id") {
                    responseId = value;
                }
            }

            let values = Object.values(updates);
            values.push(reviewId);

            const queryString = 'UPDATE REVIEW SET' + valuesString.slice(0, -1) + ' WHERE id=?';

            db.query(queryString, values, function (err, result) {
                if (!err) {
                    let review = module.exports.getReview(db, {id: responseId});
                    resolve(review)
                } else {
                    reject(err);
                }
            });
        });
    }


};