module.exports = {

    averageRatings: function (db, parameters) {
        return new Promise(function (resolve, reject) {

            db.query(
                'SELECT\n' +
                '  (case when coach_rating = \'0\'\n' +
                '    then \'Below Target\'\n' +
                '   when coach_rating = 1\n' +
                '     then \'On Target\'\n' +
                '   when coach_rating = 2\n' +
                '     then \'High Performance\'\n' +
                '   when coach_rating = 3\n' +
                '     then \'Exceptional Performance\' end) as \'name\',\n' +
                '  coach_rating                           as \'value\',\n' +
                '  sum(1)                                 as \'y\'\n' +
                '\n' +
                'from REVIEW\n' +
                'WHERE coach_rating IS NOT NULL AND year=' + parameters['year'] + ' AND end_of_year=' + parameters['end_of_year'] + '\n' +
                ('coach_id' in parameters ? ' AND coach_id=' + parameters['coach_id'] + ' ' : ' ') +
                'group by 2', null, function (err, rows) {
                    if (!err) {
                        resolve(rows);
                    } else {
                        reject(err);
                    }
                });
        });
    },

    ratingOverTime: function (db, parameters) {
        return new Promise(function (resolve, reject) {

            db.query(
                'SELECT\n' +
                '  coach_rating                       as \'data\',\n' +
                '  (case\n' +
                '   when end_of_year = 0\n' +
                '     then CONCAT(year, \' Mid Year\')\n' +
                '   when end_of_year = 1\n' +
                '     then CONCAT(year, \' End of Year\') end) as \'categories\'\n' +
                'FROM REVIEW\n' +
                'WHERE user_id = ' + parameters['user_id'] + ' AND coach_rating IS NOT NULL\n' +
                'ORDER BY CONCAT(year, end_of_year);', null, function (err, rows) {
                    if (!err) {
                        resolve(rows);
                    } else {
                        reject(err);
                    }
                });
        });
    },

    goalsCompletion: function (db, parameters) {
        return new Promise(function (resolve, reject) {

            db.query(
                'SELECT\n' +
                '  (case when complete = 0\n' +
                '    then \'Incomplete\'\n' +
                '   when complete = 1\n' +
                '     then \'Complete\' end) as \'name\',\n' +
                '  sum(1) as \'y\'\n' +
                'FROM GOAL\n' +
                'WHERE user_id = ' + parameters['user_id'] + '\n' +
                'GROUP BY 1;\n', null, function (err, rows) {
                    if (!err) {
                        resolve(rows);
                    } else {
                        reject(err);
                    }
                });
        });
    }

};