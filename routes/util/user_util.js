module.exports = {

    createUser: function (db, userData) {
        return new Promise(function (resolve, reject) {

            let queryString = 'INSERT INTO USER (';

            for (let i = 0; i < Object.keys(userData).length; i++) {
                let key = Object.keys(userData)[i];

                if (i === 0) {
                    queryString += key;
                }

                if (i > 0) {
                    queryString += ', ' + key;
                }
            }

            queryString += ') VALUES (';

            for (let i = 0; i < Object.values(userData).length; i++) {

                if (i === 0) {
                    queryString += '?';
                }

                if (i > 0) {
                    queryString += ', ?';
                }
            }

            queryString += ');';

            db.query(queryString, Object.values(userData), function(err, rows) {
                if (!err) {
                    let user = module.exports.getUser(db, {id: rows.insertId + ''});
                    resolve(user)

                } else {
                    reject(err)

                }
            });
        })
    },

    deleteUser: function (db, userId) {
        return new Promise(function (resolve, reject) {

            db.query('DELETE FROM USER WHERE id = ?', userId, function (err, result) {
                if (!err) {
                    resolve(result);
                } else {
                    reject(err);
                }
            });
        });
    },

    getUser: function (db, filter) {
        return new Promise(function(resolve, reject) {
            let queryString = 'SELECT * FROM USER';

            for (let i = 0; i < Object.keys(filter).length; i++) {
                let key = Object.keys(filter)[i];
                let value = Object.values(filter)[i];

                if (i === 0) {
                    queryString += ' WHERE ';
                }

                if (i > 0) {
                    queryString += ' AND ';
                }

                queryString += key + ' LIKE ?';
            }

            queryString += ';';

            db.query(queryString, Object.values(filter), function (err, result, fields) {
                if (!err) {
                    resolve(result);
                } else {
                    reject(err);
                }
            })
        })
    },

    updateUser: function (db, userId, updates) {
        return new Promise(function (resolve, reject) {
            let valuesString = '';
            let responseId = userId;

            for (const [key, value] of Object.entries(updates)) {
                valuesString += ' ' + key + '=?,';

                if (key === "id") {
                    responseId = key;
                }
            }

            const queryString = 'UPDATE USER SET' + valuesString.slice(0, -1) + ' WHERE id=?';

            let values = Object.values(updates);
            values.push(userId);

            db.query(queryString, values, function (err, result) {
                if (!err) {
                    let user = module.exports.getUser(db, {id: responseId});
                    resolve(user)
                } else {
                    reject(err);
                }
            })
        })
    }
};