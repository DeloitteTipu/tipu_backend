#!/usr/bin/env nodejs
const express       = require('express');
const app           = express();
const mysql         = require('mysql');
const bodyParser    = require('body-parser');
const config        = require('./config/config');

const port          = 8080;

let db = mysql.createConnection({
    host: config.host,
    database: config.database,
    user: config.username,
    password: config.password

});



app.use(bodyParser.json({extended: true}));

/*
Adds custom error handling for errors form body-parser where invalid JSON is sent
Will respond 'BAD REQUEST' to any invalid JSON
 */
app.use(function (error, req, res, next) {
    if (error instanceof SyntaxError) {
        res.sendStatus(400)
    } else {
        next();
    }
});

/*
Allows for CORS requests which will allow for it to be used by the front end
 */
app.all('*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});

app.use('/api', express.static(__dirname + '/public'));

require('./routes')(app, db);
app.listen(port, function() {
    console.log('Starting TipuAPI on port ' + port);

    db.connect(function(err) {
        if (err) throw err;
        console.log("Connected to the database successfully");
    });

    }
);